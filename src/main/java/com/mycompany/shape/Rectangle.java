/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author sairu
 */
public class Rectangle extends Shape {

    private double h;
    private double w;

    public Rectangle(double h, double w) {
        super("Rectangle");
        this.h = h;
        this.w = w;
    }

    public double getH() {
        return h;
    }

    public double getW() {
        return w;
    }

    public void setH(double h) {
        this.h = h;
    }

    public void setW(double w) {
        this.w = w;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "h = " + h + ", w = " + w + '}';
    }

    @Override
    public double calArea() {
        return h * w;
    }

}
